Solution Support Heartbeat
===============================
This repository contains a configuration based script that can be used to check the healt of Solution Support Sites

## To Run
* Copy `site-config.json.default` to `site-config.json`
* Configure the newly copied `site-config.json` with the desired values
* Run `npm install`
* Run `node index.js` to execute the main script using NodeJS

## Checks Available
* css-selctor
    * Description: This check can be used to verify that the corresponding HTML dom node is avaialble by using the CSS selector to verify.
    * Configuration Options:
        * cssSelector: A valid CSS selector to query the DOM with
* response-time
    * Description: This check can be used to verify that the URL returns within the supplied threshold.
    * Configuration Options:
        * timeToBeat: The amount of time, in Milliseconds, that consitutes a valid request
* status-code
    * Description: This check can be used to verify that the YRK returns a valid status code
    * Configuration Options:
        * validCodes: An array of valid status codes to accept