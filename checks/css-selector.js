/* use strict; */
const https = require('https')

const cheerio = require('cheerio')


exports.check = function (url, config) {
    return new Promise(
        function (resolve, reject) {
            https.get(url, function (res) {

                let body = ""
                res.on('data', (d) => {
                    body += "" + d
                }).on('end', () => {
                    const $ = cheerio.load(body)
                    if ($(config.cssSelector).length <= 0) {
                        reject({
                            name: "CSS Selector",
                            message: "Selector: " + config.cssSelector + " - Was not found "
                        })
                    } else {
                        resolve({
                            name: "CSS Selector",
                            message: "Selector: " + config.cssSelector + " - Found: " + $(config.cssSelector).length
                        })
                    }
                });
            }).on('error', function (e) {
                reject({ name: "CSS Selector", message: "An error occured: " + e })
            })
        }
    )
}