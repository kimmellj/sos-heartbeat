/* use strict; */
const https = require('https')

exports.check = function (url, config) {
    return new Promise(
        function (resolve, reject) {
            https.get(url, function (res) {
                if (config.validCodes.indexOf(res.statusCode) > -1) {
                    resolve({ name: "Status Code Check", message: "Status Code: " + res.statusCode })
                } else {
                    reject({ name: "Status Code Check", message: "An error occured: " + e })
                }
            }).on('error', function (e) {
                reject({ name: "Status Code Check", message: "An error occured: " + e })
            })
        }
    )
}