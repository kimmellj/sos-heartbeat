/* use strict; */
const https = require('https')

exports.check = function (url, config) {
    return new Promise(
        function (resolve, reject) {
            var start = Date.now();
            https.get(url, function (res) {
                let responseTime = Date.now() - start
                if (responseTime < config.timeToBeat) {
                    resolve({ name: "Response Time Check", message: "Response Time: " + responseTime + "ms" })
                } else {
                    reject({ name: "Response Time Check", message: "Response Time: " + responseTime + "ms" })
                }
            }).on('error', function (e) {
                reject({ name: "Response Time Check", message: "An error occured: " + e })
            })
        }
    )
}