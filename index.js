/* use strict; */

const async = require('async')
const colors = require('colors')

const sites = require('./site-config.json')

function runCheck (site, checkObj, callback) {
    let check = require(checkObj.file)

    checkPromise = check.check(site.url, checkObj.config).then((response) => {
        callback(null, { success: true, name: response.name, message: response.message })
    }).catch((response) => {
        callback(null, { success: false, name: response.name, message: response.message })
    })
}

function testSite (site, callback) {
    async.map(site.checks, function (check, innerCallback) {
        runCheck(site, check, innerCallback)
    }, function (err, results) {
        callback(err, { site: site.url, results: results })
    })
}

async.map(sites, testSite, (err, results) => {
    let output = "Heartbeat Results \n"
    output += "================================ \n"

    for (let i in results) {
        if (results[i]) {
            output += "Site: " + results[i].site + "\n"
            output += "--------------------------- \n"
            for (let j in results[i].results) {
                if (results[i].results[j]) {
                    /**
                     * u2713 = ✓
                     * u2718 = ✘
                     */
                    let indivResult = results[i].results[j]
                    output += "Check: " + indivResult.name + "\n"
                    output += "Result: " + ((indivResult.success) ? ("Success! ... \u2713").bgGreen : ("Failure! ... \u2718  ").bgRed) + "\n"
                    output += "Message: " + indivResult.message + "\n"
                    output += "- - - - - - - - - - - - - - - - - - - \n"
                }
            }
        }
    }
    console.log(output)
})
